function getMatchesPerYear(matches){
    let matchesPerYear ={}
    for(let i=0;i<matches.length;i++){
        let season=matches[i]["season"]
        if(!matchesPerYear.hasOwnProperty(season)){
            matchesPerYear[season]=1
        }
        else{
            matchesPerYear[season] += 1
        }
    }
    return matchesPerYear;
}

function matchesWonPerTeamPerYear(matches) {
    let matchesWonPerYear = matches.reduce((matchesWonPerYear, currentMatch) => {
      if (matchesWonPerYear.hasOwnProperty(currentMatch.season)) {
        if (matchesWonPerYear[currentMatch.season].hasOwnProperty(currentMatch.winner)) {
              matchesWonPerYear[currentMatch.season][currentMatch.winner] += 1;
        } else {
               if (currentMatch.winner != ''){
                  matchesWonPerYear[currentMatch.season][currentMatch.winner] = 1;
            }
          }
      }
      else {
          matchesWonPerYear[currentMatch.season] = {};
      }
      return matchesWonPerYear
    }, {});
  
    return matchesWonPerYear
  }


  function extraRunsInTheYear2016(matches,deliveries) {

    let getIdOf2016 = matches.filter((match) => match.season == 2016)
      .map((match) => parseInt(match.id));
  
    let extraRunsIn2016 = deliveries.reduce((extraRunsIn2016, currentBall) => {
      let matchId = parseInt(currentBall.match_id);
      if (getIdOf2016.includes(matchId)) {
        if (extraRunsIn2016.hasOwnProperty(currentBall.bowling_team)) {
          extraRunsIn2016[currentBall.bowling_team] += parseInt(currentBall.extra_runs);
        } else {
          extraRunsIn2016[currentBall.bowling_team] = parseInt(currentBall.extra_runs);
        }
      }
      return extraRunsIn2016;
    }, {})
    return extraRunsIn2016;
  }


  function topTenEconomicalBowlersIn2015(matches,deliveries){

    let getIdOf2015 = matches.filter((match) => match.season == 2015)
      .map((match) => parseInt(match.id));
  
    let allBowlers = deliveries.reduce((allBowlers, currentBall) => {
  
      let matchId = parseInt(currentBall.match_id)
      if (getIdOf2015.includes(matchId)) {
        if (allBowlers.hasOwnProperty(currentBall.bowler)) {
          allBowlers[currentBall.bowler].balls += 1;
          allBowlers[currentBall.bowler].runs += parseInt(currentBall.total_runs);
          let run = allBowlers[currentBall.bowler].runs;
          let ball = allBowlers[currentBall.bowler].balls;
          allBowlers[currentBall.bowler].economy = (run / (ball / 6)).toFixed(2);
        } else {
          allBowlers[currentBall.bowler] = { "runs": parseInt(currentBall.total_runs), "balls": 1, "economy": 0 };
        }
      }
      return allBowlers;
    }, {})
    const Economy = [];
    for(let bowlerName in allBowlers){
      if(allBowlers[bowlerName].economy <10 ){
            Economy.push(allBowlers[bowlerName].economy);
        }
    }
    Economy.sort();
    const topTenEconomyBowlers = {};
    for (let index = 0; index <=8; index++) {
      for (key in allBowlers) {
        if (allBowlers[key].economy == Economy[index]) {
          topTenEconomyBowlers[key] = { economyRate: allBowlers[key].economy };
        }
      }
    }
  
    return topTenEconomyBowlers;
  
  }


module.exports={getMatchesPerYear,matchesWonPerTeamPerYear,
    extraRunsInTheYear2016,topTenEconomicalBowlersIn2015}