
//Find the number of times each team won the toss and also won the match
function noOfTimesWonTheTossTeamAndMatches(matches){
    let getWonTossAndMatches = matches.reduce((getWonTossAndMatches, currentMatch) => {

        if (getWonTossAndMatches.hasOwnProperty(currentMatch.winner)) {
            if (currentMatch.toss_winner == currentMatch.winner) {
                getWonTossAndMatches[currentMatch.winner] += 1;
            }
        }
        else{
            if (currentMatch.toss_winner == currentMatch.winner) {
                getWonTossAndMatches[currentMatch.winner] = 1;
            }
            else{ 
                getWonTossAndMatches[currentMatch.winner] = 0;
            }
        }
        return getWonTossAndMatches;
    },{});
  
    return getWonTossAndMatches;
}
  

//Find a player who has won the highest number of Player of the Match awards for each season

function playerOfTheMatch(matches){
    let getMatchOfhePlayer ={}
    for(let i=0;i<matches.length;i++){
        let season=matches[i]["season"]
        let player=matches[i]['player_of_match']
        if(!getMatchOfhePlayer.hasOwnProperty(player,season)){
            getMatchOfhePlayer[season]=player
        }
        else{
            getMatchOfhePlayer[season]+= player
        }
    }
    return getMatchOfhePlayer
}

//Find the strike rate of a batsman for each season

function getBatsmanStrikeRate(matches,deliveries) {
    let seasons={}
    for(let i=0;i<matches.length;i++){
        let season=matches[i]["season"]
        if(!seasons.hasOwnProperty(season)){
            seasons[season]= "season"
        }
    }
    for(let year of Object.keys(seasons)) {
        let returnedObj = returnBatsmanStrikeRate(year);
        seasons[year] = returnedObj
    }
    function returnBatsmanStrikeRate(year) {
        //let getMatchesId=matches.filter(match=>match.season).map(match=>match.id)
       let getMatchesId= [];
        matches.forEach((obj) => {
            if(obj['season'] == year) {
                getMatchesId.push(obj['id'])
            }
        });
        let getMatchDeliveriesId = deliveries.filter((obj) => getMatchesId.includes(obj['match_id']));
        let batsmanArray = [];
        for(let obj of getMatchDeliveriesId) {
            if(batsmanArray.includes(obj['batsman'])) {
                continue;
            } else {
                batsmanArray.push(obj['batsman'])
            }
        }
        let batsmanObj = {}
        for(batsman of batsmanArray) {
            let runs = 0;
            let balls = 0;
            for(obj of getMatchDeliveriesId) {
                if(batsman == obj['batsman']) {
                    runs += Number(obj['batsman_runs']);
                    if(obj['wide_runs'] == 0) {
                        balls += 1;
                    }
                }
            }
            batsmanObj[batsman] = Number(((runs / balls) * 100).toFixed(2));
        }
        return batsmanObj;
    }

    return seasons;
}

//Find the highest number of times one player has been dismissed by another player

function getHighestPlayerDismissal(deliveries) {
    let pairDismissalArray = [];
    for(let obj of deliveries) {
        if(obj['player_dismissed'].length > 1) {
            pairDismissalArray.push([obj['batsman'], obj['bowler']]);
        } 
    }
    let pairDismissalCount = [];
    for(let individual of pairDismissalArray) {
        let batsman = individual[0];
        let bowler = individual[1];
        let count = 0;
        for(let whole of pairDismissalArray) {
            if(whole[0] === batsman && whole[1] === bowler) {
                count += 1;
            }
        }

        pairDismissalCount.push(count);
    }
    let maxDismissals = pairDismissalCount.reduce((a,b) => Math.max(a,b));
    let arrayIndexes = [];
    for(let index = 0; index < pairDismissalCount.length; index++) {
        if(pairDismissalCount[index] === maxDismissals) {
            arrayIndexes.push(index)
        }
    }
    let comboArray = [];
    for(let index of arrayIndexes) {
        comboArray.push(pairDismissalArray[index]);
    }
    let stringArray = comboArray.map(JSON.stringify);
    let uniqueStringArray = new Set(stringArray);
    let uniqueArray = Array.from(uniqueStringArray, JSON.parse);
    let resultOfMaxDismissalsObj = {}
    for(let index = 0; index < (comboArray.length / maxDismissals); index++) {
        let key = "pair" + (index + 1);
        let batsman = uniqueArray[index][0];
        let bowler = uniqueArray[index][1];

        resultOfMaxDismissalsObj[key] = {
            'batsman': batsman,
            'bowler': bowler,
            'dismissals' : maxDismissals
        }    
    }
    return resultOfMaxDismissalsObj;
}


//Find the bowler with the best economy in super overs

function superOverBowlerEconomy(deliveries){
    let economyBowlers = deliveries.reduce((economyBowlers, match) => {
      if (economyBowlers.hasOwnProperty(match.bowler)) {
        if (match.is_super_over == '1') {
          economyBowlers[match.bowler].balls += 1
          economyBowlers[match.bowler].runs += parseInt(match.total_runs);
          let run = economyBowlers[match.bowler].runs
          let ball = economyBowlers[match.bowler].balls
          economyBowlers[match.bowler].economy = (run / (ball / 6)).toFixed(2);
        }
      }
      else {
        if (match.is_super_over == '1') {
          economyBowlers[match.bowler] = { "runs": parseInt(match.total_runs), "balls": 1, "economy": 0 };
        }
      }
      return economyBowlers
    }, {})
    let economyOnly = Object.values(economyBowlers).map((valuesOfEconomyBowlers) => parseFloat(valuesOfEconomyBowlers.economy));
   
    const economyOfTop = economyOnly.filter((economyOfBowler) => {
      if (economyOfBowler < 10) {
        return economyOfBowler
      }
    })
    economyOfTop.sort()
    const topEconomy = {}
    for (let key in economyBowlers) {
      if (economyBowlers[key]["economy"] == economyOfTop[0]) {
        topEconomy[key] = economyOfTop[0];
      }
    }
    return topEconomy;
}

module.exports={noOfTimesWonTheTossTeamAndMatches,playerOfTheMatch,getBatsmanStrikeRate,
    getHighestPlayerDismissal,superOverBowlerEconomy}